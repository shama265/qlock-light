const Alert = {
  props: ['type'],
  template: `<div class="alert alert-dismissible" :class="'alert-' + type">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
    <slot></slot>
</div>`,
};

const AlertArea = {
  props: {
    alerts: Array,
  },
  components: {
    Alert,
  },
  template: `<div>
  <alert v-for="a in alerts" :type="a.type">
    <span v-html="a.message"></span>
  </alert>
</div>`,
};

const VIEW_LIMIT_DEFAULT = 10;
const VIEW_LIMIT_PLUS = 10;

const app = {
  el: '#app',
  components: {
    AlertArea,
  },
  data: {
    stamps: [],
    dateFrom: null,
    dateTo: null,
    viewLimit: VIEW_LIMIT_DEFAULT,
    alerts: [],
    datePattern: /(\d{4})-(\d{2})-(\d{2})/,
    preferences: {
      onLoadStamp: false,
    },
  },
  created: function() {
    if (!storageAvailable('localStorage')) {
      this.alerts.push({
        type: 'danger',
        message: 'Oops, \
        <a class="alert-link" href="https://developer.mozilla.org/ja/docs/Web/API/Web_Storage_API" target="_blank">Local Storage</a> is unavailable, \
        this app will not work well.'});
      return;
    }
    let storage = window.localStorage;
    let storageStamp = storage.getItem('stamps');
    if(storageStamp) {
      this.stamps = JSON.parse(storageStamp);
    } else {
      this.alerts.push({
        type: 'primary',
        message: '<strong>Note:</strong>\
        If you are using a web browser with private(incognito) mode, \
        your records will not be saved.\
        <a class="alert-link" href="https://developer.mozilla.org/docs/Web/API/Web_Storage_API#Private_Browsing_Incognito_modes" target="_blank">more info</a>'
      });
    }

    let preferences = storage.getItem('preferences');
    if(preferences) {
      preferences = JSON.parse(preferences);
      this.preferences['onLoadStamp'] = !!preferences['onLoadStamp'];
      this.preferences['autoReadMore'] = !!preferences['autoReadMore'];
    }

    if(this.preferences.onLoadStamp === true) {
      this.stamp(true);
    }
  },
  watch: {
    stamps: 'saveStamps',
    dateFrom: 'resetLimit',
    dateTo: 'resetLimit',
  },
  computed: {
    stampsView: function() {
      let view = this.stamps;
      let from = this.dateFromNorm;
      let to = this.dateToNorm;
      if(from) {
        view = view.filter(d => d >= new Date(from.year, from.monthIdx, from.date));
      }
      if(to) {
        view = view.filter(d => d <= (new Date(to.year, to.monthIdx, to.date + 1)));
      }
      return view;
    },
    stampsSliced: function() {
      return this.stampsView.slice(0, this.viewLimit);
    },
    dateFromNorm: function() {
      return this.parseDate(this.dateFrom);
    },
    dateToNorm: function() {
      return this.parseDate(this.dateTo);
    },
  },
  filters: {
    timestamp: function (value, type) {
      var date = new Date(value);
      return date.toLocaleString();
    }
  },
  methods: {
    stamp: function(quiet) {
      this.stamps.unshift(Date.now());
      this.viewLimit++;
      if(!quiet) {
        $('#stamped').show().fadeOut(1000);        
      }
    },
    saveStamps: function(newStamps) {
      window.localStorage.setItem('stamps', JSON.stringify(newStamps));
    },
    readMore: function() {
      this.viewLimit += VIEW_LIMIT_PLUS;
    },
    resetLimit: function() {
      this.viewLimit = VIEW_LIMIT_DEFAULT;
    },
    parseDate: function(dateString) {
      let result = this.datePattern.exec(dateString);
      if(result) {
        result = {
          year: parseInt(result[1]),
          monthIdx: parseInt(result[2]) - 1,
          date: parseInt(result[3]),
        };
      }
      return result;
    },
    infiniteHandler: function($state) {
      if (this.viewLimit < this.stampsView.length) {
        this.readMore();
        $state.loaded();  
      } else {
        $state.complete();
      }
    }
  },
};

const ToggleButton = {
  props: {
    isOn: Boolean,
  },
  template: `<button class="btn"
  :class="{ 'btn-primary': isOn, 'btn-secondary': !isOn}"
  @click="$emit('click')"
  v-html="isOn ? '<strong>On</strong>' : 'Off'"></button>`,
};

const settings = {
  components: {
    AlertArea, ToggleButton
  },
  data: function() {
    return {
      alerts: [],
      preferences: {
        onLoadStamp: false,
        autoReadMore: false,
      },
    };
  },
  created: function() {
    if (!storageAvailable('localStorage')) {
      this.alerts.push({
        type: 'danger',
        message: 'Oops, \
        <a class="alert-link" href="https://developer.mozilla.org/ja/docs/Web/API/Web_Storage_API" target="_blank">Local Storage</a> is unavailable, \
        this app will not work well.'});
      return;
    }
    let storage = window.localStorage;
    let preferences = storage.getItem('preferences');
    if(preferences) {
      preferences = JSON.parse(preferences);
      this.preferences['onLoadStamp'] = !!preferences['onLoadStamp'];
      this.preferences['autoReadMore'] = !!preferences['autoReadMore'];
    }
  },
  watch: {
    preferences: {
      handler: 'savePreferences',
      deep: true,
    },
  },
  computed: {
    stampDataSize: function() {
      let result = 0;
      if(storageAvailable) {
        let storage = window.localStorage;
        let storageStamp = storage.getItem('stamps');
        if(storageStamp) {
          result = storageStamp.length;
        }
      }
      return result;
    },
  },
  filters: {
    byte: function (value) {
      const units = ['Byte', 'kB', 'MB'];
      let border = 0x400;
      let result = value;
      let i = 0; // index of units
      while (i < units.length - 1 && value >= border) {
        result = value / border;
        i++;
        border <<= 10;
      }

      // fix number, expect Byte scale
      if(i > 0) {
        if(result >= 100) {
          result = result.toFixed(); // 123 UN
        } else if(result >= 10) {
          result = result.toFixed(1); // 12.3 UN
        } else {
          result = result.toFixed(2); // 1.23 UN
        }
      }

      return `${result} ${units[i]}`;
    },
  },
  methods: {
    savePreferences: function(newPrefs) {
      window.localStorage.setItem('preferences', JSON.stringify(newPrefs));
    },
    togglePreference: function(name) {
      this.preferences[name] = !this.preferences[name];
    },    
    deleteAll: function() {
      if(confirm('Your stamp data will be never recovered!\nAre you sure you want to delete all records?')) {
        window.localStorage.removeItem('stamps');
        this.alerts.push({
          type: "success",
          message: "Complete to delete your all records."
        });
      }
    },
  }
};

/**
 * Web Storage Availability Checker
 * from MDN web docs
 * https://developer.mozilla.org/ja/docs/Web/API/Web_Storage_API/Using_the_Web_Storage_API
 */
function storageAvailable(type) {
    var storage;
    try {
        storage = window[type];
        var x = '__storage_test__';
        storage.setItem(x, x);
        storage.removeItem(x);
        return true;
    }
    catch(e) {
        return e instanceof DOMException && (
            // everything except Firefox
            e.code === 22 ||
            // Firefox
            e.code === 1014 ||
            // test name field too, because code might not be present
            // everything except Firefox
            e.name === 'QuotaExceededError' ||
            // Firefox
            e.name === 'NS_ERROR_DOM_QUOTA_REACHED') &&
            // acknowledge QuotaExceededError only if there's something already stored
            (storage && storage.length !== 0);
    }
}
