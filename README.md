# QLOCK-light #

QLOCK-light is a very simple time-stamping web application.
All timestamps are saved to your computer, without connecting internet.
You can try it on [my website](https://rzna.work/qlock-light/).

### Features ###

* Record timestamps locally

### Usage ###

* Click "STAMP" button to record time when clicking.
* Records are saved in your [local storage](https://developer.mozilla.org/docs/Web/API/Web_Storage_API).
* If you want, you can also delete all timestamp records.

------

# QLOCK-light #

QLOCK-light は、非常にシンプルなタイムスタンプWebアプリケーションです。
すべてのタイムスタンプはあなたのコンピューター上に保存され、インターネットと通信することはありません。
[作者のWebサイト](https://rzna.work/qlock-light/)で動作を確認できます。

### 機能

* ローカルにタイムスタンプを記録

### 使い方

* "STAMP"ボタンをクリックすると、クリックした時刻が記録されます。
* 記録はあなたの[ローカルストレージ](https://developer.mozilla.org/docs/Web/API/Web_Storage_API)に保存されます。
* 望むならば、タイムスタンプの記録をすべて削除することもできます。
